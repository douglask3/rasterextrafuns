\name{FUN.gunzip}
\alias{FUN.gunzip}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
FUN.gunzip
}
\description{
%%  ~~ A concise (1-5 lines) description of what the function does. ~~
}
\usage{
FUN.gunzip(FUN, filename, lon_range = NULL, lat_range = NULL, ...)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{FUN}{
%%     ~~Describe \code{FUN} here~~
}
  \item{filename}{
%%     ~~Describe \code{filename} here~~
}
  \item{lon_range}{
%%     ~~Describe \code{lon_range} here~~
}
  \item{lat_range}{
%%     ~~Describe \code{lat_range} here~~
}
  \item{\dots}{
%%     ~~Describe \code{\dots} here~~
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function (FUN, filename, lon_range = NULL, lat_range = NULL,
    ...)
{
    if (exists("ptm")) {
        cat("\ntime since start of last call:\n")
        print(proc.time() - ptm)
    }
    ptm <<- proc.time()
    print(filename)
    install_and_source_library("R.utils")
    ext = substr(filename, nchar(filename) - 2, nchar(filename))
    if (ext == ".gz" || ext == "zip") {
        if (ext == ".gz")
            filename_unzip = substr(filename, 1, nchar(filename) -
                3)
        else filename_unzip = substr(filename, 1, nchar(filename) -
            4)
        try(gunzip(filename, destname = filename_unzip))
        out = FUN(filename_unzip, ...)
    }
    else out = FUN(filename, ...)
    if (nlayers(out) == 1)
        out = out[[1]]
    if (!is.null(lon_range) && !is.null(lat_range)) {
        nl = nlayers(out)
        if (nl == 1)
            out = out[[1]]
        iby = findidDenominator(nl)
        out = layer.apply(1:(nl/iby), crop.mem.safe, iby, out,
            extent(lon_range[1], lon_range[2], lat_range[1],
                lat_range[2]))
    }
    if (ext == ".gz" || ext == "zip") {
        out = out * 1
        try(gzip(filename_unzip))
    }
    cat("time to open file:\n")
    print(proc.time() - ptm)
    return(out)
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
