\name{add_raster_legend2}
\alias{add_raster_legend2}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{add_raster_legend2}
\description{
%%  ~~ A concise (1-5 lines) description of what the function does. ~~
}
\usage{
add_raster_legend2(cols, limits, labelss = NULL, x = "bottomleft", dat = NULL, add = TRUE, spt.cex = 2, pch = 15, main_title = "", plot_loc = c(0.22, 0.7, 0.07, 0.1 + 0.05 * (length(e_lims) - 1)), e_lims = 0, labelss.cex = 1, title_cex = 0.8, srt = 90, transpose = TRUE, invert_e = TRUE, polygons = TRUE, elim_lab = TRUE, elim_txt = TRUE, oneSideLabels = TRUE, ylabposScling = 1, ...)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{cols}{
%%     ~~Describe \code{cols} here~~
}
  \item{limits}{
%%     ~~Describe \code{limits} here~~
}
  \item{labelss}{
%%     ~~Describe \code{labelss} here~~
}
  \item{x}{
%%     ~~Describe \code{x} here~~
}
  \item{dat}{
%%     ~~Describe \code{dat} here~~
}
  \item{add}{
%%     ~~Describe \code{add} here~~
}
  \item{spt.cex}{
%%     ~~Describe \code{spt.cex} here~~
}
  \item{pch}{
%%     ~~Describe \code{pch} here~~
}
  \item{main_title}{
%%     ~~Describe \code{main_title} here~~
}
  \item{plot_loc}{
%%     ~~Describe \code{plot_loc} here~~
}
  \item{e_lims}{
%%     ~~Describe \code{e_lims} here~~
}
  \item{labelss.cex}{
%%     ~~Describe \code{labelss.cex} here~~
}
  \item{title_cex}{
%%     ~~Describe \code{title_cex} here~~
}
  \item{srt}{
%%     ~~Describe \code{srt} here~~
}
  \item{transpose}{
%%     ~~Describe \code{transpose} here~~
}
  \item{invert_e}{
%%     ~~Describe \code{invert_e} here~~
}
  \item{polygons}{
%%     ~~Describe \code{polygons} here~~
}
  \item{elim_lab}{
%%     ~~Describe \code{elim_lab} here~~
}
  \item{elim_txt}{
%%     ~~Describe \code{elim_txt} here~~
}
  \item{oneSideLabels}{
%%     ~~Describe \code{oneSideLabels} here~~
}
  \item{ylabposScling}{
%%     ~~Describe \code{ylabposScling} here~~
}
  \item{\dots}{
%%     ~~Describe \code{\dots} here~~
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function (cols, limits, labelss = NULL, x = "bottomleft", dat = NULL,
    add = TRUE, spt.cex = 2, pch = 15, main_title = "", plot_loc = c(0.22,
        0.7, 0.07, 0.1 + 0.05 * (length(e_lims) - 1)), e_lims = 0,
    labelss.cex = 1, title_cex = 0.8, srt = 90, transpose = TRUE,
    invert_e = TRUE, polygons = TRUE, elim_lab = TRUE, elim_txt = TRUE,
    oneSideLabels = TRUE, ylabposScling = 1, ...)
{
    if (length(cols) != length(limits) + 1)
        cols = make_col_vector(cols, ncols = length(limits) +
            1)
    if (!add) {
        plot.new()
        add = TRUE
    }
    if (is.null(e_lims)) {
        e_lims = 0
        cols_e = make.transparent("grey", 1 - 1/(length(e_lims) +
            1))
    }
    cal_frac_of_plot_covered <- function(index = 1:2) {
        xlims = par("usr")[index]
        xlims[3] = xlims[2] - xlims[1]
        return(xlims)
    }
    if (add) {
        xlims = cal_frac_of_plot_covered(1:2)
        ylims = cal_frac_of_plot_covered(3:4)
    }
    else {
        xlims = c(0, 1, 1)
        ylims = c(0, 1, 1)
        plot(c(0, 1), c(0, 1))
    }
    nlims = length(limits) + 1
    x = xlims[1] + plot_loc[1] * xlims[3] + (plot_loc[2] - plot_loc[1]) *
        xlims[3] * (matrix(1:nlims, nlims, 1) - 0.5)/nlims
    y = seq(ylims[1] + ylims[3] * plot_loc[3], ylims[1] + ylims[3] *
        plot_loc[4], length.out = length(e_lims) + 1)
    z = matrix(1:nlims, nlims, 1 + length(e_lims))
    `:=`(c(xx, yy, z), transpose_cor(transpose, x, y, z))
    image(x = xx, y = yy, z = z, col = cols[1:nlims], add = TRUE,
        xaxt = "n", yaxt = "n", xlab = "", ylab = "", xdp = TRUE)
    if (length(e_lims) > 1) {
        z = t(matrix(1:(1 + length(e_lims)), 1 + length(e_lims),
            nlims))
        if (invert_e)
            z = invert(z)
        image_z_gt_i <- function(i, pch, cex, pattern, thick,
            res) {
            zz = z
            zz[z > i] = 1
            zz[z <= i] = 0
            `:=`(c(x, y, zz), transpose_cor(transpose, x, y,
                zz))
            xx = rep(x, length(y))
            yy = rep(y, each = length(x))
            zz = as.vector(zz)
            cols = c("transparent", make.transparent("black",
                1 - 0.5 * i/length(e_lims)))
            cols_e = make.transparent("black", 1 - 0.75 * i/length(e_lims))
            if (polygons) {
                zz = rasterFromXYZ(cbind(xx, yy, zz))
                image(pattern.raster(disaggregate(zz, 100), pattern,
                  , thick = thick, res = res * 25), col = c("transparent",
                  cols_e), add = TRUE)
            }
            else points(xx, yy, col = cols[zz + 1], pch = pch,
                cex = cex * 1.7)
        }
        for (i in 1:length(e_lims)) image_z_gt_i(i, c(4, 3, 1,
            16, 5, 9, 11)[i], c(1, 1, 1.3, 1.3, 1.3, 1, 1)[i],
            c("Circle", "Circle", "forward-diagonal", "backward-diagonal",
                "horizontal", "vertical")[i], c(0.1, 0.67, 0.5,
                0.5, 0.5, 0.5)[i], c(16, 8, 4, 4, 4, 4)[i])
    }
    dx = (x[2] - x[1])/2
    xp = c(min(x) - dx, min(x) - dx, max(x) + dx, max(x) + dx)
    yp = c(y[1] - diff(range(y))/(length(e_lims) * 2), tail(y,
        1) + diff(range(y))/(length(e_lims) * 2))
    `:=`(c(xx, yy), transpose_cor(transpose, xp, c(yp, rev(yp))))
    polygon(xx, yy, xpd = TRUE)
    yl = seq(yp[1], yp[2], length.out = length(e_lims) + 2)
    if (!polygons && length(e_lims) > 1) {
        if (!transpose)
            lapply(yl, function(y) lines(xp[2:3], rep(y, 2),
                lwd = 0.5, col = make.transparent("blacK", 0.5)))
        else lapply(yl, function(y) lines(rep(y, 2), xp[2:3],
            lwd = 0.5, col = make.transparent("blacK", 0.5)))
    }
    ytop = tail(yp, 1) + tail(diff(y), 1) * ylabposScling
    ybottom = yp[1] - diff(y[1:2] * ylabposScling)
    xt = c(x[1] - dx, x[1], x + dx)
    if (oneSideLabels)
        ytop = ybottom
    yt = c(ytop, rep(c(ytop, ybottom), length.out = length(xt) -
        1))
    if (is.null(labelss)) {
        lab1 = ""
        labN = ""
        dlim1 = diff(limits[1:2])
        dlimN = diff(rev(tail(limits, 2)))
        if (!is.null(dat)) {
            if (limits[1] > 0 && limits[1] <= dlim1 && min.raster(dat,
                na.rm = TRUE) >= 0)
                lab1 = "0"
            else if (tail(limits, 1) < 0 && tail(limits, 1) <=
                dlimN && max.rater(dat, na.rm = TRUE) <= 0)
                labN = "0"
        }
        labelss = c(lab1, "", limits, labN)
        if (limits[1] == 0) {
            labelss[1] = ""
            labelss[2] = "0"
            labelss[3] = ""
        }
    }
    else {
        if (class(labelss) == "list" && length(labelss) == 1)
            labelss = labelss[[1]]
        if (limits[1] == 0) {
            labelss = c("", labelss[1], "", labelss[-1], "")
        }
        else {
            labelss = c(labelss[1], "", labelss[-1], "")
        }
        if (length(labelss) == (length(xt) - 2))
            labelss = c("", labelss, "")
        if (length(labelss) == (length(xt) - 1))
            labelss = c("", "", labelss[-2])
    }
    if (length(labelss) == 0)
        labelss = rep("", length(xt))
    `:=`(c(xx, yy), transpose_cor(transpose, xt, yt))
    if (transpose) {
        srt = srt - 90
        adj = 1
    }
    else {
        adj = 0.5
        yy = yy - 0.1
    }
    text(x = xx + 0.0667 * (length(e_lims) - 1), y = yy, labelss,
        xpd = NA, cex = labelss.cex, srt = srt, adj = adj)
    if (length(e_lims) != 1) {
        `:=`(c(xx, yy), transpose_cor(transpose, xt[1] - 2 *
            diff(xt[1:2]), c(y[1] - diff(y[1:2]), y) - 0.33 *
            c(diff(y[1:3]), diff(y))/2))
        srt = 0
        if (transpose) {
            srt = srt + 90
            xx = xx + diff(xx[1:2])
        }
        if (elim_txt)
            text(x = xx, y = yy - 0.05, c(0, e_lims, ""), pos = 3,
                cex = labelss.cex * 0.95, xpd = NA, srt = srt)
        if (elim_lab)
            mtext("sd/mean (\%)", side = c(2, 1)[transpose * 1 +
                1], cex = title_cex, line = -1 + transpose *
                1, xpd = NA)
    }
    for (i in list(c(1, 3), c(2))[[transpose * 1 + 1]]) mtext(main_title,
        side = i, cex = title_cex, line = -1 - transpose * 0.1)
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
